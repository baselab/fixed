<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

  <head>
    <title><?php echo $page_name." @ ".$site_name; ?></title>
    <link rel="stylesheet" type="text/css" href="<?php echo $style_url; ?>" />
    <link rel="shortcut icon" href="<?php echo $icon_url; ?>" />
    <meta http-equiv="Content-type" content="text/html;charset=UTF-8" />
    <meta http-equiv="Content-Language" content="en" />
    <meta name="description" content="fixed site" />
    <meta name="keywords" content="fixed" />
    <meta name="author" content="cretox" />
    <meta name="generator" content="My hands, my brain and some stuff from the web" />
  </head>
  
  <body>
    <div id="pageblock">  
    <div id="header"></div>
    <div id="header_text">fixed</div>
