<?php
include ("config/siteurl.php");

/* Begin of configuration */

$home_url = "index.php";
$site_name = "fixed";
$style_url = "include/style2.css";
$icon_url = "images/favicon.ico";

$nav_url["home"]=$home_url;
$nav_url["who"]=$home_url."?id=who";
$nav_url["why"]=$home_url."?id=why";
$nav_url["where"]=$home_url."?id=where";

/*allowed id input*/
$id_whitelist = array("where", "who", "why");

/* End of configuration */


$raw_id = $_GET["id"];

/*sanitize user input*/
$id = preg_replace("/\W/", "", $raw_id);

if ($id && in_array($id, $id_whitelist)) {
  $page_name = $id;
  $inc = "include/".$id.".php";
}
elseif ($id && $id!="home") {
  $page_name = "404";
  $inc = "include/404.php";
}
else {
  $page_name = "home";
  $inc = "include/home.php";
}  

include ("include/header.php");
include ("include/navigator.php");

echo "<div id=\"content\">\n";

include ($inc);

echo "</div>\n";

include ("include/footer.php");

?>